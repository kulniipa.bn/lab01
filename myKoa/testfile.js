let assert = require("assert");
const fs = require("fs");

describe('Array', function() {
    describe('#indexOf()', function() {
      it('should return -1 when the value is not present', function() {
        assert.equal([1,2,3].indexOf(4), -1);
      });
    });
});

  
describe("Array", function() {
  describe("#testObject()", function() {
    it("should be equal to this object structure", function() {
        let myVariable = { a: 1, b: { c: [1, 2, 3] } };
        assert.deepEqual(myVariable,{ a: 1, b: { c: [1, 2, 3] } },"Obj myVariable");
    });
  });
});

function myReadFile2(filename) {
  return new Promise(function(resolve, reject) {
    fs.readFile(filename, "utf8", function(err, dataDemo1) {
      if (err) reject(err);
      else resolve(dataDemo1);
    });
  });
}

describe("Async Test2", function() {
  describe("#myReadFile2()", function() {
    it("should read content of the file out", async function() {
        const data = await myReadFile2("demofile1.txt");
        assert.strictEqual(data.length > 0,true,"demofile1.txt should not be empty");
    });
  });
});

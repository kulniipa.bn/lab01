function draw1(n){
    let star="";
    for(let i=0;i<n;i++){
        star += "*"
    }
    console.log(star);
}

function draw2(n){
    let star="";
    for(let i=0; i<n; i++){
        for(let j=0; j<n; j++){
            star += "*"
        }
        star += '\n'
    }
    console.log(star);
}

function draw3(n){
    let number="";
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number += j
        }
        number += '\n'
    }
    console.log(number);
}

function draw4(n){
    let number="";
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number += i
        }
        number += '\n'
    }
    console.log(number);
}

function draw5(n){
    let number="";
    for(let i=n; i>=1; i--){
        for(let j=n; j>=1; j--){
            number += i
        }
        number += '\n'
    }
    console.log(number);
}

function draw6(n){
    let number="";
    let number2="";
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number2++
            number += number2
        }
        number += '\n'
    }
    console.log(number);
}

function draw7(n){
    let number="";
    let number2=n*n;
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            number += number2
            number2--
        }
        number += '\n'
    }
    console.log(number);
}

function draw8(n){
    let result="";
    for(let i=0; i<n; i++){
        result += i*2 + '\n';
    }
    console.log(result);
}

function draw9(n){
    let result="";
    for(let i=1; i<=n; i++){
        result += i*2 + '\n'
    }
    console.log(result);
}

function draw10(n){
    let result="";
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            result += j*i;
        }
        result += '\n'
    }
    console.log(result);
}

function draw11(n){
    let result="";
    let mark="";
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            (j==i) ? mark = "-" : mark = "*";
            result += mark
        }
        result += '\n'
    }
    console.log(result);
}

function draw12(n){
    let result="";
    let mark="";
    for(let i=0; i<n; i++){
        for(let j=1; j<=n; j++){
            ((n-i)==j) ? mark = "-" : mark = "*"
            result += mark
        }
        result += '\n';
    }
    console.log(result);
}

function draw13(n){
    let result="";
    let mark="";
    for(let i=1; i<=n; i++){
        for(let j=1; j<=n; j++){
            (i>=j) ? mark = "*" : mark ="-";
            result += mark;
        }
        result += '\n'
    }
    console.log(result);
}

function draw14(n){
    let result="";
    let mark="";
    for(let i=1; i<=n; i++){
        for(let j=0; j<n; j++){
            ((n-i) >= j) ? mark = "*" : mark ="-";
            result += mark;
        }
        result += '\n';
    }
    console.log(result);
}

function draw15(n){
    let result='';
    for(let i=1; i<=(n+(n-1)); i++){
        for(let j=1; j<=n; j++){
            result += "*"
        }
        result += '\n'
    }
    console.log(result);
}
draw15(2)
draw15(3)
draw15(4)


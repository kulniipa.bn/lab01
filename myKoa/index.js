const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const fs = require('fs')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async ctx => {
    await ctx.render('landing')
})

router.get('/sorting', async ctx => {
    const arr = [3,44,38,5,47,15,36,27,2,46,4,19,50,48];
    let minIndex = 0;
    let temp;
    for (let i=1; i<arr.length; i++) {
       if (arr[i] < arr[minIndex])
            minIndex = i;
            console.log(minIndex);
            arr = swap(arr,i,minIndex);
    }
    console.log(arr);
    console.log(temp);
    console.log(arr);
    //arr = [33,1,44,1,32,62,2,11,22,33] 
    //index 4  >  32
    //index2 = 7 > 11

    function swap(arr, index, index2) {
        let temp;
        temp = arr[index]; 32
        arr[index] = arr[index2];
        //arr[index] = 11
        arr[index2] = temp;
        //arr[index2] = 32
        //arr = 1,2,3,6,5,4

        //arr = [1,2,3,6,5,6]
    }     
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
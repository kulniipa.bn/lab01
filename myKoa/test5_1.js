let assert = require('assert');
let add = require('./add.js');
let myAdd = add.myAdd;

describe('tdd lab', function() {
  describe('#add()', function() {
    it('should calculate 1+1=2', function() {
      assert.deepEqual(myAdd(1,1), 2);
    });
    it('should calculate 2+1=2', function() {
        assert.deepEqual(myAdd(2,1), 3);
      });
  });
});

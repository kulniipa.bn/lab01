const fs = require("fs");

let head = new Promise(function(resolve, reject) {
  fs.readFile("head.txt", "utf8", function(err, data) {
    if (err) {
      reject(err);
    }
    resolve(data);
  });
});

let body = new Promise(function(resolve,reject){
    fs.readFile("body.txt","utf8",function(err,data){
        if(err){
            reject(err);
        }
        resolve(data);
    })
})

let leg = new Promise(function(resolve,reject){
    fs.readFile("leg.txt","utf8",function(err,data){
        if(err){
            reject(err);
        }
        resolve(data);
    })
})

let feet = new Promise(function(resolve,reject){
    fs.readFile("feet.txt","utf8",function(err,data){
        if(err){
            reject(err);
        }
        resolve(data);
    })
})

function writeRobot(robot) {
    return new Promise(function(resolve, reject) {
      fs.writeFile('robot3.txt', robot, 'utf8', function(err) {
        if (err)
          reject(err);
        else
          resolve();
      });
    });
}

async function myRobot(){
    let result = await Promise.all([head, body, leg, feet]);
    let robot = "";
    result.forEach(function(result){
        console.log(result);
        robot += `${result}\n`;
    })
    await writeRobot(robot);
}

myRobot();

/*Promise.all([head, body, leg, feet])
.then(function(result){
  console.log('All completed!: ', result);
  writeRobot(result);
})
.catch(function(error){
  console.error("There's an error", error); 
});*/

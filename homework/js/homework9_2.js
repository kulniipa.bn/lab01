'use strict'

let fs = require('fs');

let head = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', (err, DataHead) => {
        if (err)
            reject(err);
        else
            resolve(DataHead + "\n");
    });
});

let body = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', (err, DataBody) => {
        if (err)
            reject(err);
        else
            resolve(DataBody + "\n");
    });
});

let leg = new Promise((resolve, reject) => {
    fs.readFile('leg.txt', 'utf8', function (err, DataLeg) {
        if (err)
            reject(err);
        else
            resolve(DataLeg + "\n");
    });
});

let feet = new Promise((resolve, reject) => {
    fs.readFile('feet.txt', 'utf8', function (err, DataFeet) {
        if (err)
            reject(err);
        else
            resolve(DataFeet);
    });
});

async function ReadWriteFiles() {
    const result = await Promise.all([head, body, leg, feet])
    fs.writeFile('robot.txt', result, 'utf8', err => {
        console.log("Write Success!");
    });
}
ReadWriteFiles();
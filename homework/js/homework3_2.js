Employees();
function Employees(){
    $(document).ready(function() {
        let employees;
        fetch('data/homework2_1.json').then(response => {
            return response.json();
        })
        .then(myJson => {
            employees=myJson;
            AddYearSalary(employees[0]);
            AddNextSalary(employees[0]);
            let newEmployees = addAdditionalFields(employees);
            addAdditionalFields(newEmployees);
            
            console.log(newEmployees);
            console.log(employees);

            //create NewEmployeeTable
            $('#NewEmployeeTable').append($('<tr>'));
            for (let k in newEmployees[0]) {
                $('#NewEmployeeTable').append($('<th>' + k + '</th>'));    
            }
            $('#NewEmployeeTable').append($('</tr>'));

            let list;
            for (let i in newEmployees) {
                $('#NewEmployeeTable').append($('<tr>'));
                for (let j in newEmployees[i]) {
                
                        if(j==="nextSalary"){
                            list='<ol>';
                            for(let x=0;x<newEmployees[i][j].length;x++){
                                //console.log(newEmployees[i][j]);
                                let salary=newEmployees[i][j];
                                list+='<li>' + salary[x] + '</li>';
                            }
                            list+='</ol>';
                        }else if(j!=="nextSalary"){
                            list=newEmployees[i][j];
                        }
                        $('#NewEmployeeTable').append($('<td>' + list + '</td>'));
                
                }
                $('#NewEmployeeTable').append($('</tr>'));
            }
            //create NewEmployeeTable
        })
        .catch(error => {
            console.error('Error:', error);
        });



        let YearSalary;
        function AddYearSalary(objEmployee){
            YearSalary=objEmployee['salary'];
            objEmployee.yearSalary=YearSalary*12;   
        }

        function AddNextSalary(objEmployee){
            let year_salary=[];
            let y1,y2,y3;
            let NextSalary=[];
            
            NextSalary=objEmployee['salary'];
            objEmployee.nextSalary=NextSalary;

            y2=objEmployee['salary'];
            //console.log(objEmployee['salary']);
            for (let a=0;a<=2;a++){
                if(a==0){
                    year_salary[a]=y2;
                }else if(a>0){
                    y2+=y2*0.1;
                    year_salary[a]=y2;
                }
            }
            objEmployee.nextSalary=year_salary;
        }


        function addAdditionalFields(ObjEmployees){
            let employee;
            let header;
            for(let i in ObjEmployees){
                employee=ObjEmployees[i];
                AddYearSalary(employee);
                AddNextSalary(employee);
            }

            //create EmployeeTable
            $('#EmployeeTable').append($('<tr>'));
            for (let k in ObjEmployees[0]) {
                //console.log(k);
                $('#EmployeeTable').append($('<th>' + k + '</th>'));    
            }
            $('#EmployeeTable').append($('</tr>'));

            let list;
            for (let i in ObjEmployees) {
                //console.log(ObjEmployees[i]);
                $('#EmployeeTable').append($('<tr>'));
                for (let j in ObjEmployees[i]) {
                
                        if(j==="nextSalary"){
                            list='<ol>';
                            for(let x=0;x<ObjEmployees[i][j].length;x++){
                                //console.log(ObjEmployees[i][j]);
                                let salary=ObjEmployees[i][j];
                                list+='<li>' + salary[x] + '</li>';
                            }
                            list+='</ol>';
                        }else if(j!=="nextSalary"){
                            list=ObjEmployees[i][j];
                        }
                        $('#EmployeeTable').append($('<td>' + list + '</td>'));
                
                }
                $('#EmployeeTable').append($('</tr>'));
            }
            //create EmployeeTable

            let newEmployees=[];
            let CloneObj={};
            CloneObj.id=ObjEmployees[0].id;
            CloneObj.firstname=ObjEmployees[0].firstname;
            CloneObj.lastname=ObjEmployees[0].lastname;
            CloneObj.company=ObjEmployees[0].company;
            CloneObj.salary=ObjEmployees[0].salary;
            CloneObj.yearSalary=ObjEmployees[0].yearSalary;
            CloneObj.nextSalary=ObjEmployees[0].nextSalary;
            newEmployees[0]= CloneObj;
            newEmployees[1] = ObjEmployees[1];
            newEmployees[2] = ObjEmployees[2];
            newEmployees[3] = ObjEmployees[3];
            newEmployees[0].salary = 0 

            return newEmployees;
        }

    });
}
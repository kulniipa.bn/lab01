$(function () {
    fetch('data/homework1.json').then(response => {
            return response.json();
        })
        .then(myJson => {
            let peopleSalary = myJson;
            console.log(peopleSalary);

            //filter salary
            const peopleLowSalary = peopleSalary.filter((emp) => emp.salary < 100000);
            console.log(peopleLowSalary);

            //up salary
            const AddSalary = peopleLowSalary.map((emp) => emp.salary * 2);
            //conditon map&reduce
            console.log(AddSalary);

            //sum salary
            const sumSalary = AddSalary.reduce((sum, number) => {
                return sum + number;
            }, 0)
            //console.log(sumSalary);



            //*Sum Salary*        
            function EmpMap(emp) {
                let answer = emp.salary < 100000 ? emp.salary * 2 : emp.salary
                return answer;
            }

            function EmpReduce(sum, emp) {
                return sum + emp;
            }
            const Salary = peopleSalary
                .map(EmpMap)
                .reduce(EmpReduce);
            console.log(Salary);
        })
        .catch(error => {
            console.error('Error:', error);
        });
});
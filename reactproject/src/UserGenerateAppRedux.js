import React, { Component } from "react";
import { getUser } from "./actions";
import { connect } from "react-redux";

class UserGenerateAppRedux extends Component {
  getRandomUser = async props => {
    const response = await fetch("https://randomuser.me/api/");
    const strUser = await response.json();

    const strPicture = strUser.results[0].picture.large;
    const strEmail = strUser.results[0].email;
    const strGender = strUser.results[0].gender;
    const strName = strUser.results[0].name.title + " " + strUser.results[0].name.first + " " + strUser.results[0].name.last;

    console.log(strPicture)
    //props.getUser(strPicture, strEmail, strGender, strName);
    this.props.getUser(strUser.results[0]);
  };

  render() {
    return (
      <form>
        <img src={this.props.picture} />
        <p>Email : {this.props.email}</p>
        <p>Gender : {this.props.gender}</p>
        <p>Name : {this.props.name}</p>
        <button onClick={this.getRandomUser}>get</button>
      </form>
    );
  }
}

/*<img src={this.props.picture} />
<p>Email : {this.props.email}</p>
<p>Gender : {this.props.gender}</p>
<p>Name : {this.props.name}</p>
<button onClick={this.getRandomUser}>get</button>*/

//export {UserGenerateAppRedux}
const mapStateToProps = state => {
    console.log(state)
    return {
        picture: state.picture,
        email: state.email,
        gender: state.gender,
        name: state.name
    };
};

export default connect(
  mapStateToProps,
  getUser
)(UserGenerateAppRedux);

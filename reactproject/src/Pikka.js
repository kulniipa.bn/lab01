import React from 'react'
import {PictureCardById} from './PictureCardById'
import {Footer} from './Footer'
import './PictureCardById.css'

class Pikka extends React.Component{
    state ={
        id: '',
        createdAt: '',
        createdBy: '',
        picture: '',
        likeCount: '',
        commentCount: '',
        comments:'',
        isLoading: true
    }
    async componentDidMount() {
        try {
            const pothoID = this.props.match.params.id
            const response = await fetch('http://35.240.130.197/api/v1/pikka/' + pothoID)
            const detail = await response.json()
            console.log(detail)
            this.setState({
                id:detail.id,
                createdAt:detail.createdAt,
                createdBy:detail.createdBy,
                picture:detail.picture,
                likeCount:detail.likeCount,
                comments:detail.comments,
                commentCount:detail.comments.length
            })
          } catch (err) {
            console.log(err.message);
          }finally {
            this.setState({ isLoading: false });
          }   
    }

    render(){  
        console.log(this.state)
        const {id,createdAt,createdBy,picture,likeCount,commentCount,comments,isLoading} = this.state
        console.log(this.props)
        console.log(this.props.match.params.id)
        const { matchUrl } = this.props;
        if (isLoading) return <div>Loading...</div>;
        return(
            <div>
                <header className="App-header">
                </header>
                <div className="App-intro">   
                    <div className="box">                 
                        <PictureCardById 
                            id={id}
                            createdAt={createdAt}
                            createdBy={createdBy}
                            picture={picture}
                            likeCount={likeCount}
                            commentCount={commentCount}
                            comments={comments}
                        />
                    </div>                 
                </div>
                <div className="App-Footer">
                    <Footer/>
                </div>
            </div>
        )
    }
}

export {Pikka}
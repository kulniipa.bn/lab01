import React from 'react'

const Logo = () => <img src='https://raw.githubusercontent.com/panotza/pikkanode/master/pikkanode.png' alt='pikkanode'/>
export {Logo}
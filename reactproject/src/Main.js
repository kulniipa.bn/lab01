import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import {Dashboard} from './Dashboard'
import {SignUp} from './pages/SignUp'
import SignIn from './pages/SignIn'
import {SignOut} from './pages/SignOut'
import {CreatePikka} from './pages/CreatePikka'
import {createStore} from "redux"
import {Provider} from "react-redux"
import {rootReducer} from "./reducer"
import { Pikka } from './Pikka';

const store = createStore(rootReducer)
const Main = (props) => (
  <Provider store={store}>
    <Switch>
      <Route path='/' exact render={() => <Dashboard />} />
      <Route path='/pikka/:id' component={Pikka} />
      <Route path='/create' render={(props) => <CreatePikka history={props.history}/>}  />
      <Route path='/signup' render={() => <SignUp />} />
      <Route path='/signin' component={SignIn}/>} />
      <Route path='/signout' render={() => <SignOut />} />
    </Switch>
  </Provider>
)

export default Main
function rootReducer(state={isAuth:false},action){
    console.log(state)
    switch(action.type){
        case 'AUTHEN':
            return {...state,isAuth:action.isAuth}
        default:
            return state
    }
}

function userGenerateReducer(state = { picture: '',email:'',gender:'',name:'' },action){
    console.log(state)
    switch(action.type){
        case 'USER':
            return {...state,email:action.email,gender:action.gender,name:action.name}
        default:
            return state
    }
}

function incrementByReducer(state = { count: 0 }, action) {
    switch (action.type) {
      case 'INCREMENT':
        return { ...state, count: state.count + action.amount }
      default:
        return state
    }
  }
  
export {
    rootReducer,
    userGenerateReducer,
    incrementByReducer
}
import React, { Component } from "react";
import "./App.css";
import { Navbar } from "./Navbar.js";
import Main from "./Main.js";
//import {PictureCard} from './PictureCard.js'
import { BrowserRouter } from "react-router-dom";

class Pikkanode extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar/>
          <Main/>
        </div>
      </BrowserRouter>
    );
  }
}

export default Pikkanode


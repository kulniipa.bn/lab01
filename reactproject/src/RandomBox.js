import React,{Component} from 'react'

class RandomBox extends Component{
    getRandomInt = (min, max) =>{
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }
    render(){
        const randomFontSize = this.getRandomInt(20,40)
        const arrColor = ['red', 'blue', 'green', 'purple', 'pink']
        const randomColor = arrColor[Math.floor(Math.random() * arrColor.length)];
        return(
            <div style={{backgroundColor:randomColor, width:300,height:300,margin:'auto'}}>
                <p style={{fontSize:randomFontSize, color: 'white',textAlign:'center',paddingTop:120,fontWeight:'bold'}}>Random Box</p>
            </div>    
        )
    }
}

export {RandomBox}
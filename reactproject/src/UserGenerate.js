import React,{Component} from 'react'
import UserGenerateAppRedux from './UserGenerateAppRedux'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {userGenerateReducer} from './reducer'

const store = createStore(userGenerateReducer)
class UserGenerate extends Component{
    render(){
        return(
            <Provider store={store}>
                <UserGenerateAppRedux/>
            </Provider>
        )
    }
}

export {UserGenerate}
import React,{Component} from 'react'

class CardDect extends Component{
    getColor = (e) => {
        alert(e.target.id)
        console.log(e.target.name)
    }

    //set state
    state = {
        id: ''
    }
    handleClick = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    render(){
        const arrColor = ['red', 'blue', 'green', 'purple', 'pink'] 
        const dbColor = [...arrColor,...arrColor]
        const cards = this.props.suffle(dbColor)
        return (
            <div style={{textAlign:'center'}}>
                    <div style={{display:'flex',justifyContent:'start',flexWrap:'wrap'}}>
                        {cards.map(card => 
                            <div 
                                name={card} 
                                id={card}
                                onClick={this.getColor} 
                                style={{backgroundColor:card, width:200,height:300,margin:10}}>
                            </div>
                        )}
                    </div>
                    <button onClick={this.handleClick} >new deck</button>
            </div>
        )
    }
}

export {CardDect}

module.exports = {
    async createEntity(row){
        return {
            id:row.id,
            name:row.name,
            price:row.price,
            detail:row.detail,
            teach_by:row.teach_by
        }
    },
    async getInstructorAll(pool){
        const [rows] = await pool.query("SELECT * FROM instructors")
        return rows.map(this.createEntity)
    },
    async getInstructorById(pool,id){
        const [rows] = await pool.query("SELECT * FROM instructors WHERE id=?",[id])
        return this.createEntity(rows[0])
    }
}

module.exports = {
    async createEntity(row){
        return {
            id:row.id,
            name:row.name,
            price:row.price,
            jsonData:JSON.parse(row.json_data)
        }
    },
    async getCourseAll(pool){
        const [rows] = await pool.query("SELECT * FROM courses")
        return rows.map(this.createEntity)
    }
}

const Koa = require('koa')

const app = new Koa()

app.use(ctx=>{
    ctx.status = 401
    console.log(ctx.headers)
    ctx.set('Content-Type', 'text/html; charset=utf-8')
    ctx.set('Cache-Control', 'private, max-age=0')
    //ctx.body = '<h1>Hello World</h1>'
    //console.log(ctx.request)
    console.log(ctx.response)
    
})
app.listen(3000)
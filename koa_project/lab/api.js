const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

//render ejs
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async (ctx, next) => {
    await ctx.render('facebook_api')
})

app.use(router.routes())
app.listen(3000)
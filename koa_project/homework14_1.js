'use strict'
const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool  = mysql.createPool({
   connectionLimit : 10,
   host            : 'localhost',
   user            : 'root',
   password        : 'nI<5!!My$q1',
   database        : 'codecamp'
});

router.get('/',async ctx =>{
    const results = await getAllPriceEnrolls()
    let objData = {}
    objData.course = results

    const results2 = await getSumPriceEnrollOfStudent();
    let obj = {}
    obj.instructor = results2
    console.log({course:objData.course,instructor:obj.instructor})
    await ctx.render('homework14_1',{course:objData.course,instructor:obj.instructor})
})

async function getAllPriceEnrolls() {
    let [rows, fields] = await pool.query(`SELECT sum(courses.price) as price
                                            FROM enrolls
                                            inner join courses on courses.id=enrolls.course_id`);
    console.log("COURSE : ",rows)
    return rows;
}

async function getSumPriceEnrollOfStudent() {
    let [rows, fields] = await pool.query(`SELECT students.name AS student_name,SUM(courses.price) AS price
                                            FROM enrolls
                                            INNER JOIN courses ON courses.id=enrolls.course_id
                                            INNER JOIN students ON students.id=enrolls.student_id
                                            GROUP BY students.name`);
    console.log("INSTRUCTORS : ",rows)
    return rows;
}

app.use(serve(path.join(__dirname,"public")))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(5000)


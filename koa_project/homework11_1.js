'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool  = mysql.createPool({
   connectionLimit : 10,
   host            : 'localhost',
   user            : 'root',
   password        : 'nI<5!!My$q1',
   database        : 'codecamp'
});

router.get('/users',async ctx =>{
    const results = await getUser();
    let objData = {};
    objData.user = results
    console.log(results);
    await ctx.render('homework11_1',objData)
})

async function getUser() {
    let [rows, fields] = await pool.query('SELECT * FROM user');
    console.log('The solution is: ', rows, fields);
    return rows;
}

app.use(serve(path.join(__dirname,"public")))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(4000)

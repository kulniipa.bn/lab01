const {Employee} = require('./employee.js');

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    gossip(employee,conversation){
        console.log('Hey ' + employee.firstname + ' ' + conversation)
    }
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        //employee.setSalary(employee._salary)
        let compareSalary = employee.setSalary(newSalary) 
    }
    _fire(){
        this.dressCode = 'tshirt';
        console.log("Somsri has been fired! Dress with :" + this.dressCode); 
    }
    _hire(){
        this.dressCode = 'tshirt';
        console.log("Somsri has been hired back! Dress with :" + this.dressCode);         
    }
    _seminar(){
        this.dressCode = 'suit';
        console.log("He is going to seminar Dress with :" + this.dressCode); 
    }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);       
    };

}
exports.CEO = CEO;
'use strict'
const Koa = require('koa')
const Router = require('koa-router')

const app = new Koa()
const router = new Router()

const pool = require('../lib/db.js')

router.get('/instructor/find_all',async (ctx,next)=>{
    const [rows] = await pool.query('SELECT * FROM instructors')
    ctx.body = rows
    await next()
})

router.get('/instructor/find_by_id/:id',async (ctx,next)=>{
    const [rows] = await pool.query('SELECT * FROM instructors WHERE id=?',[ctx.params.id])
    ctx.body = rows
    await next()
})

router.get('/course/find_by_id/:id',async (ctx,next)=>{
    const [rows] = await pool.query('SELECT * FROM courses WHERE id=?',[ctx.params.id])
    ctx.body =  rows
    await next()
})

router.get('/course/find_by_price/:price',async (ctx,next)=>{
    const [rows] = await pool.query("SELECT * FROM courses WHERE price=?",[ctx.params.price])
    ctx.body = rows
    await next()
})

app.use(router.routes())
app.listen(5000)
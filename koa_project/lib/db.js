'use strict'

const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'nI<5!!My$q1',
    database: 'codecamp'
});

module.exports = pool;

'use strict'
const pool = require('../lib/db')

async function courseDetail() {
    let [rows, fields] = await pool.query(`SELECT students.name AS student_name,courses.name AS course_name,
                                            (
                                            CASE
                                            WHEN courses.teach_by IS NULL
                                                THEN 'No Instructor'
                                                ELSE instructors.name
                                            end
                                            ) AS instructors,
                                            courses.detail AS detail,courses.price AS price
                                            FROM students
                                            INNER JOIN enrolls ON enrolls.student_id=students.id
                                            INNER JOIN courses ON courses.id=enrolls.course_id
                                            LEFT JOIN instructors ON instructors.id=courses.teach_by
                                            UNION
                                            SELECT 'Total','','',sum(price),'' from courses
                                            INNER JOIN enrolls ON courses.id=enrolls.course_id`)
    return rows
}
module.exports = courseDetail;

'use strict'

const fs = require('fs')
function getData() {
    return new Promise(function (resolve, reject) {
        fs.readFile('homework2_1.json', 'utf8', function (err, DataLeg) {
            if (err) {
                reject(err);
                return
            }
            resolve(DataLeg);
        });
    });
}

async function readFiles() {
    try {
        const strData = await getData();
        return strData

    } catch (error) {
        console.error(error);
    }
}

module.exports = readFiles;
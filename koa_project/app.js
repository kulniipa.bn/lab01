'use strict'
const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

const pool = require('./lib/db.js')
const getUser = require('./models/from_database')
const readFiles = require('./models/from_file')
const courseDetail = require('./models/homework14_3')

const Employee = require('./models/class.js')
//const employees = new Employee()

//render ejs
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

//homework11_3.js
router.get('/from_database', async (ctx, next) => {
    const results = await getUser()
    ctx.body = results
    ctx.myVariable = results
    await next()
})

router.get('/from_file', async (ctx, next) => {
    const result = await readFiles()
    ctx.body = JSON.parse(result)
    ctx.myVariable = JSON.parse(result)
    await next()
})

async function additionalDB(ctx, next) {
    let objData = ctx.myVariable
    let obj = {}
    let objAdditional = {}
    objAdditional.userId = 1
    let dt = new Date()
    objAdditional.dateTime = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate() +
        " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds()
    obj.data = ctx.myVariable
    obj.additionalData = objAdditional
    ctx.body = obj
    await next()
}

//homework14_3.js
router.get('/courses', async ctx => {
    const results = await courseDetail();
    let objData = {};
    objData.course = results
    console.log(results);
    await ctx.render('homework14_3', objData)
})

//lab_day16
router.get('/hello/:name', async (ctx, next) => {
    ctx.body = 'Hello ' + ctx.params.name + '!';
    await next();
});
router.get('/hello/:name/:lastname', async (ctx, next) => {
    ctx.body = 'Hello ' + ctx.params.name + ' ' + ctx.params.lastname + '!';
    await next();
});
router.get('/normal_query/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * from users WHERE id = ?',[ctx.params.id]);
    ctx.body = rows;
    await next();
});

app.use(router.routes())
app.use(additionalDB)

app.use(serve(path.join(__dirname, "public")))
app.use(router.allowedMethods())
app.listen(4000)